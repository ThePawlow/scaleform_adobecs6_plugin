Write-Host "[Setup] Welcome!"
Write-Host "[Setup] Verifying destination folders"
[String]$cs6ConfigDir = "\Adobe\Flash CS6\en_US\Configuration"

[String]$destCommands = Join-Path -Path  $env:LOCALAPPDATA -ChildPath "$cs6ConfigDir\Commands"
if (!(Test-Path $destCommands))
{
    throw "Commands folder was not found for Flash CS6 at $destCommands"
}

[String]$destExternalLibs = Join-Path -Path  $env:LOCALAPPDATA -ChildPath "$cs6ConfigDir\External Libraries"
if (!(Test-Path $destExternalLibs))
{
    throw "External Libary folder was not found for Flash CS6 at $destExternalLibs"
}

[String]$destPublishProfile = Join-Path -Path  $env:LOCALAPPDATA -ChildPath "$cs6ConfigDir\Publish Profiles"
if (!(Test-Path $destPublishProfile))
{
    throw "Publish Profiles was not found for Flash CS6 at $destPublishProfile"
}

Write-Host "[Setup] Copying files..."
Copy-Item "./src/External Libraries/*" $destExternalLibs
Copy-Item "./src/Commands/*" $destCommands
Copy-Item "./src/Publish Profiles/*" $destPublishProfile

Write-Host "[Setup] Adjusting available commands..."
$availableCommands = Join-Path -Path $destCommands -ChildPath "names.xml"

if (!(Test-Path $availableCommands))
{
    Write-Warning "No available commands present - That's odd, we'll create one for you."
    $xmlContent = @"
<?xml version="1.0" encoding="utf-8"?>
<Commands>
  <name source="Build All" target="Build All" />
  <name source="Build as GFX" target="Build as GFX" />
</Commands>
"@
    # Save the initial XML content to the file
    $xmlContent | Out-File -FilePath $availableCommands -Encoding utf8
}
else
{
    [xml]$xml = Get-Content $availableCommands
    function Contains-Source
    {
        param (
            [System.Xml.XmlNodeList]$nodeList,
            [string]$sourceValue
        )

        foreach ($node in $nodeList)
        {
            if ($node.source -eq $sourceValue)
            {
                return $true
            }
        }
        return $false
    }

    $entries = @("Build All", "Build as GFX")

    foreach ($entry in $entries)
    {
        if (-not (Contains-Source -nodeList $xml.Commands.ChildNodes -sourceValue $entry))
        {
            $newName = $xml.CreateElement("name")
            $newName.SetAttribute("source", $entry)
            $newName.SetAttribute("target", $entry)
            $xml.Commands.AppendChild($newName) | Out-Null

            Write-Host "[Setup] Added '$entry' successfully."
        }
    }

    $xml.Save($availableCommands)

}

