/**
 * Created with JetBrains Rider.
 * User: Pawlow
 * Date: 12/05/2024
 * Time: 13:20
 */
const documents = fl.documents;
fl.trace("[Publishing] Found " + documents.length);

if(documents.length === 0)
{
	fl.trace("[Publishing] No documents found");
}
else
{
	for each(var document in documents) {
		fl.trace("Publishing Document");
		document.publish();
	}
	fl.trace("Finished publishing...");
}