try {
    scaleform
} catch (e) {
    if (e.name === "ReferenceError") {
        throw "Failed to load the Scaleform Extension";
    }

    throw e
}

var currentDocument = fl.getDocumentDOM()
if (currentDocument === null) {
    throw "There is no document open to publish";
}

fl.trace("Building")
fl.publishDocument(currentDocument.pathURI, "GTAVScaleform");

var response = scaleform.runGfxExporter(currentDocument.path)
fl.trace(response)